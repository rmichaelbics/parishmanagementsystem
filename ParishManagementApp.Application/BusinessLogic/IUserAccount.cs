using ParishManagementApp.Core.Entities;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using ParishManagementApp.Application.Interface;
using ParishManagementApp.Infrastructure.Repositories;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;



namespace ParishManagementApp.Application.BusinessLogic
{

    public interface IUserAccount 
    {
    // using ParishManagementApp.Application.Interfaces;
         Task<Users> AuthenticateUser(Users users);   
    }

    public class UserAccountRepo: IUserAccount
    {
        private readonly IUserRegisterRepository _userRegisterRepository;
        public UserAccountRepo(IUserRegisterRepository userRegisterRepository)
        {
            this._userRegisterRepository = userRegisterRepository;
        }

      
        public async Task<Users> AuthenticateUser(Users users)
        {
           var result= await _userRegisterRepository.AuthenticateUser(users);

            return result;
        //    if(result!=null){

        //         return new Users() {
        //             Username = result.Username,
        //             UserId = result.UserId,
        //             RoleId = result.RoleId,
        //             EmailAddress = result.EmailAddress,
        //             RoleName =result.RoleName,
        //             UserRegisterId = result.UserRegisterId                       
        //         };
        //    }
        //    return null;
        }

      
    }
}