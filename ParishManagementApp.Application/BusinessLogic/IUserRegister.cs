using ParishManagementApp.Core.Entities;
using ParishManagementApp.Application.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;
using ParishManagementApp.Infrastructure.Repositories;

namespace ParishManagementApp.Application.BusinessLogic
{
    public interface IUserRegister : IGenericRepository<UserRegister>
    {
         
    }

    public class UserRegisterRepo : IUserRegister
    {
        private readonly IUserRegisterRepository _userRegRepository;
        public UserRegisterRepo(IUserRegisterRepository userRegisterRepository){
            this._userRegRepository = userRegisterRepository;
        }

        public Task<int> Add(UserRegister entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> Delete(int? id)
        {
            throw new System.NotImplementedException();
        }

        public Task<UserRegister> Get(int? id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<UserRegister>> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> Update(UserRegister entity)
        {
            throw new System.NotImplementedException();
        }
    }
}