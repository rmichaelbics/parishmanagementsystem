using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using ParishManagementApp.Application.BusinessLogic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

using ParishManagementApp.Core.Entities;
using ParishManagementApp.Api.Helper;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace ParishManagementApp.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUserAccount _userRepository;

        public LoginController(IConfiguration configuration, IUserAccount userRepository)
        {
            this._configuration = configuration;
            this._userRepository = userRepository;
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string userName, string password){

            IActionResult response = Unauthorized();
            var users = new Users();
            users.Username = userName;
            users.Password =password;
            var user = await AuthenticateUser(users);
            if(user !=null){
                var token = new JWTAuthendication(_configuration).GenerateJWTWebToken(user);

                response = Ok(new {token=token});
            }
            return response;
        }

        // [Authorize]
        [HttpGet("GetValue")]
        public ActionResult<IEnumerable<string>> Get(){

            return new  string[]{"Value1","Value2"} ;
        }

        // [Authorize]
        [HttpPost("Post")]
        public string Post(){
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            return "Welcome to: " + identity.Claims.ToList()[0].Value;

        }
        private async Task<Users> AuthenticateUser(Users users){
            
            if(users != null){
                return await _userRepository.AuthenticateUser(users);                
            }
            return null;
        }
    }
}