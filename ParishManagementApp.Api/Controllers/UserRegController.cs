using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ParishManagementApp.Application.BusinessLogic;

using ParishManagementApp.Core.Entities;

namespace ParishManagmentApp.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UserRegController: ControllerBase
    {
        private readonly IUserRegister _userRegRepository;
        public UserRegController(IUserRegister userRegRepository )
        {
            this._userRegRepository = userRegRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<UserRegister>> GetAll()
        {
                return await _userRegRepository.GetAll();
        }
         [HttpGet("{id}")]
        public async Task<UserRegister> Get(int? id)
        {
            return await _userRegRepository.Get(id);
        }
        [HttpPost]
        public async Task<int> AddUser(UserRegister users)
        {
            return await _userRegRepository.Add(users);
        }

        [HttpPut]
        public async Task<int> UpdateUser(UserRegister users)
        {
            return await _userRegRepository.Update(users);
        }

        [HttpDelete("{id}")]
        public async Task<int> DeleteUser(int? id)
        {
            return await _userRegRepository.Delete(id);
        }
    
    }
}