using Microsoft.Extensions.DependencyInjection;
using ParishManagementApp.Application.BusinessLogic;
namespace ParishManagementApp.Api.Helper
{
    public static class DependencyInjectionService
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services){
            
            services.AddTransient<IUserAccount , UserAccountRepo>();
            services.AddTransient<IUserRegister, UserRegisterRepo>();
            return services;
        }
    }
}