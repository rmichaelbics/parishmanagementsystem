using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace ParishManagementApp.Api.Helper
{
    public static class Swagger
    {
        public static IServiceCollection AddSwagger( this IServiceCollection service)
        {
            service.AddSwaggerGen(c =>
                    {
                        c.SwaggerDoc("v1", new OpenApiInfo {
                             
                           Version = "v1",
                           Title = "ToDo API",
                        //    Description = "Parish Management System",
                        //    TermsOfService = new Uri("https://example.com/terms"),
                           
                         });

                         c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
                            In = ParameterLocation.Header, 
                            // Description = "Please insert JWT with Bearer into field",
                            // Name = "Authorization",
                            Type = SecuritySchemeType.ApiKey 
                        });
                        c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                        { 
                            new OpenApiSecurityScheme 
                            { 
                            Reference = new OpenApiReference 
                            { 
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer" 
                            } 
                            },
                            new string[] { } 
                            } 
                        });
                          // Set the comments path for the Swagger JSON and UI.
                        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                        c.IncludeXmlComments(xmlPath);
                    });
                return service;
        }

        public static IApplicationBuilder AddSwaggerUI(this IApplicationBuilder app){
            app.UseSwagger();
               // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });
             return app;   
        }
    }
}