using System.Configuration;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ParishManagementApp.Core.Entities;
using Microsoft.IdentityModel.JsonWebTokens;
using System.IdentityModel.Tokens.Jwt;
using System;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace ParishManagementApp.Api.Helper
{
    public  class JWTAuthendication
    {
        private readonly IConfiguration configuration;

        public JWTAuthendication(){

        }
        public JWTAuthendication(IConfiguration configuration){
            this.configuration = configuration;
        }
        public string GenerateJWTWebToken(Users users){
            
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JWTSecretKey:Token"]));
            var credentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256);

            var claims = new []
            {
                new Claim(JwtRegisteredClaimNames.Sub, users.Username),
                new Claim(JwtRegisteredClaimNames.Email, users.EmailAddress),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role,users.RoleName)
            };

            var Tokens = new JwtSecurityToken(
                issuer: configuration["JWTSecretKey:Issuer"],
                audience: configuration["JWTSecretKey:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials:credentials
            );
            var encodedToken = new JwtSecurityTokenHandler().WriteToken(Tokens);

            return encodedToken;
        }
    }
}