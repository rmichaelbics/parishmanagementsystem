namespace ParishManagementApp.Core.Entities
{
    public class UserRole
    {
        public int RoleId {get;set;}
        public string RoleName { get; set; }

        public int Status { get; set; }
    }
}