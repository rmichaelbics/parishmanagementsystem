using System;

namespace ParishManagementApp.Core.Entities
{
    public class UserRegister
    {
        public int UserRegId { get; set; }
        public string Firstname {get;set;}
        public string Lastname { get; set; }
        public string EmailAddress { get; set; }
         public bool IsWhatsapp { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}