using System;
namespace ParishManagementApp.Core.Entities
{
    public class Users
    { 
        public int UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string EmailAddress {get;set;}

        public string RoleName { get; set; }
        public int RoleId { get; set; }

        public UserRegister userRegister {get;set;}
        
        public UserRole userRole {get;set;}
        public int UserRegisterId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}