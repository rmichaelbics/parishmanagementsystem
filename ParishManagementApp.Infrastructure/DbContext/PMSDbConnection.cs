namespace ParishManagementApp.Infrastructure.DbContext
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using MySql.Data.MySqlClient;
    using ParishManagementApp.Infrastructure.Database;
    using System.Linq;

    public class PMSDbConnection :  IDatabaseRepository
    {
         private readonly IConnectionStringRepository _connectionStringRepository;
        public PMSDbConnection( IConnectionStringRepository connectionStringRepository ){
            this._connectionStringRepository = connectionStringRepository;
        }
        public async Task<T> Get<T>(string sql, int? data)
        {
            using (var connection = new MySqlConnection(_connectionStringRepository.ConnectionString))
            {
                connection.Open();
                var result= await  connection.QueryAsync<T>(sql,data);
                connection.Close();
                return result.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<T>> GetAll<T>(string sql)
        {
            using (var connection = new MySqlConnection(_connectionStringRepository.ConnectionString))
            {
                connection.Open();
                var result= await  connection.QueryAsync<T>(sql);
                connection.Close();
                return result;
            }
        }

        public async Task<T> GetById<T>(string sql, T data)
        {
            using (var connection = new MySqlConnection(_connectionStringRepository.ConnectionString))
            {
                connection.Open();
                var result= await  connection.QueryAsync<T>(sql, data);
                connection.Close();
                return result.FirstOrDefault();
            }
        }

        public async Task<int> SaveData<T>(string sql, T data)
        {
            int RowId =0;
             using (var connection = new MySqlConnection(_connectionStringRepository.ConnectionString))
            {
                connection.Open();
                RowId= await  connection.ExecuteAsync(sql,data);
                connection.Close();
            }
            return RowId;
        }
    }
}