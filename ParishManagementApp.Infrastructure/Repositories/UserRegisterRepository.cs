namespace ParishManagementApp.Infrastructure.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ParishManagementApp.Core.Entities;
    using System;
    using ParishManagementApp.Infrastructure.Database;
    using ParishManagementApp.Infrastructure.Interfaces;

    public interface IUserRegisterRepository : IGenericRepository<UserRegister>
    {
        Task<Users> AuthenticateUser(Users users);   
    }

    public class UserRegisterRepository : IUserRegisterRepository 
    {
        private  IDatabaseRepository _databaseRepository;
        public UserRegisterRepository(IDatabaseRepository databaseRepository) 
        {
            this._databaseRepository = databaseRepository;
        }

        public async Task<int> Add(UserRegister entity)
        {
            entity.CreatedAt = DateTime.Now;
            var sql = @"INSERT INTO userregister(Firstname, Lastname, EmailAddress, ContactNumber,IsWhatsapp,Status,CreatedAt)
                        VALUES(@Firstname, @Lastname, @EmailAddress, @ContactNumber,@IsWhatsapp,@Status,@CreatedAt)";
            
                return await  _databaseRepository.SaveData(sql,entity);
        }

        public async Task<Users> AuthenticateUser(Users users)
        {
            var sql =  @"SELECT U.UserId, U.Username, UR.EmailAddress, R.RoleName, R.RoleId, U.UserRegisterId from User AS U
                        INNER JOIN USERREGISTER AS UR ON UR.UserRegId =  U.UserRegisterId 
                        INNER JOIN ROLE AS R ON R.ROLEID =U.ROLEID
                        WHERE U.STATUS =1 AND U.USERNAME = @Username
                        AND PASSWORD =@Password";
            
            return await _databaseRepository.GetById(sql,users);
        }

        public async Task<int> Delete(int? id)
        {
            var sql = "DELETE FROM userregister WHERE USERID =@id";
            
            return await _databaseRepository.SaveData(sql, new {id = id});
        }

        public async Task<UserRegister> Get(int? id)
        {
           var sql = "SELECT * FROM userregister WHERE USERID =@UserId";
           return await _databaseRepository.GetById<UserRegister>(sql, new UserRegister(){ UserRegId = (int) id});
        }

        public async Task<IEnumerable<UserRegister>> GetAll()
        {
                 var sql = "SELECT * FROM userregister";
                 return  await _databaseRepository.GetAll<UserRegister>(sql);
        }

        public async Task<int> Update(UserRegister entity)
        {
             var sql = @"UPDATE userregister SET Firstname=@Firstname, Lastname=@Lastname, EmailAddress = @EmailAddress,
                         ContactNumber=@ContactNumber, IsWhatsapp=@IsWhatsapp, Status=@Status, CreatedAt=@CreatedAt 
                         WHERE USERID=@UserId";
            return await _databaseRepository.SaveData(sql, entity);
        }
    }
}