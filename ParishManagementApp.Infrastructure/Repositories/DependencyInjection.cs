using System.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ParishManagementApp.Infrastructure.Database;
using ParishManagementApp.Infrastructure.DbContext;

namespace ParishManagementApp.Infrastructure.Repositories
{
    public static class DependencyInjection
    {
         public static IServiceCollection AddInfrastructureServices(this IServiceCollection services){
             services.AddTransient<IConnectionStringRepository, ConnectionString>();
            services.AddTransient<IDatabaseRepository, PMSDbConnection>();
            services.AddTransient<IUserRegisterRepository, UserRegisterRepository>();
            //services.AddTransient<IUserRegister, UserRegister>();
            //services.AddTransient<IUnitofWork, UnitofWork>();
            return services;
        }
    }
}