

namespace ParishManagementApp.Infrastructure.Repositories
{
    public class UnitofWork : IUnitofWork
    {
       public UnitofWork(IUserRegisterRepository _userRepository )
        {
            userRepository = _userRepository;
        }
        public IUserRegisterRepository userRepository {get;}
    }
}
