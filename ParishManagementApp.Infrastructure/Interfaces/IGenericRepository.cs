namespace ParishManagementApp.Infrastructure.Interfaces
{
    using System.Collections.Generic;
    
    using System.Threading.Tasks;

    public interface IGenericRepository<T> where T : class
    {
        Task<T> Get(int? id);
        Task<IEnumerable<T>> GetAll();
        
        Task<int> Add(T entity);

        Task<int> Update(T entity);

        Task<int> Delete(int? id);
    }
}