namespace ParishManagementApp.Infrastructure.Database
{
    using Microsoft.Extensions.Configuration;

    public class ConnectionString :IConnectionStringRepository
    {
        private IConfiguration _configuration;

        public  ConnectionString(IConfiguration Configuration)         {
            _configuration = Configuration;
        }

        string IConnectionString.ConnectionString { get => _configuration.GetSection("ConnectionString").GetSection("DefaultConnection").Value;  }
    }
}