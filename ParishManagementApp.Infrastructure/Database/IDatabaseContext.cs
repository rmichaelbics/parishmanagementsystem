namespace ParishManagementApp.Infrastructure.Database
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface IDatabaseContext
    {
        Task<int> SaveData<T>(string sql, T data) ;
        Task<IEnumerable<T>> GetAll<T>(string sql);
        Task<T> Get<T>(string sql, int? id);
    }
}