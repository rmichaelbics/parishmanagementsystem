namespace ParishManagementApp.Infrastructure.Database
{
    using System.Threading.Tasks;
    public interface IDatabaseRepository: IDatabaseContext
    {
         Task<T> GetById<T>(string sql, T data);     
    }
}