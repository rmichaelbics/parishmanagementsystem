namespace ParishManagementApp.Infrastructure.Database
{
    public interface IConnectionString
    {
         string ConnectionString { get; }
    }
}